module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"  //Adding comments
  version = "1.50.0"

  name = "prod-vpc"
  cidr = "10.10.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b"]
  private_subnets = ["10.10.11.0/24"]

  database_subnets = ["10.10.12.0/24"]

  enable_nat_gateway               = true
  single_nat_gateway               = false
  enable_vpn_gateway               = false
  enable_dhcp_options              = true
  dhcp_options_domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    Terraform = "true"
    Environment = "prod"
  }
}
